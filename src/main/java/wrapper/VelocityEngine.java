package wrapper;

import org.apache.velocity.context.Context;
import org.apache.velocity.exception.VelocityException;

import java.io.StringWriter;

public class VelocityEngine {

    private final org.apache.velocity.app.VelocityEngine engine;

    public VelocityEngine() {
        engine = new org.apache.velocity.app.VelocityEngine();
    }

    public String merge(String template, Context context) {

        StringWriter sw = new StringWriter();
        boolean succeeded = engine.evaluate(context, sw, "Wrapper", template);
        if (!succeeded) throw new VelocityException("evaluation failed. see velocity log");

        return sw.toString();
    }

}
