package wrapper;

import org.apache.velocity.context.Context;

import java.util.Map;

public class ContextBuilder {

    private final Context context = new org.apache.velocity.VelocityContext();

    public static ContextBuilder of() {
        return new ContextBuilder();
    }

    public ContextBuilder pair(String key, Object value) {
        context.put(key, value);
        return this;
    }

    public ContextBuilder with(Map<String, Object> another) {
        another.forEach(context::put);
        return this;
    }

    public Context build() {
        return context;
    }

}
