package sample;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.StringWriter;
import java.util.Properties;

public class Bootstrap {

    public static void main(String[] args) {

        Properties properties = new Properties();
        properties.put("resource.loader", "loader");
        properties.put("loader.resource.loader.class", ClasspathResourceLoader.class.getName());

        VelocityEngine engine = new VelocityEngine(properties);
        engine.init();

        Template template = engine.getTemplate("template.vm");

        VelocityContext context = new VelocityContext();
        context.put("name", "Velocity");

        StringWriter stringWriter = new StringWriter();
        template.merge(context, stringWriter);

        System.out.println(stringWriter.toString());

    }

}
