package wrapper;

import org.apache.velocity.context.Context;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class VelocityEngineTest {

    private VelocityEngine engine;

    @Before
    public void setUp() {
        this.engine = new VelocityEngine();
    }

    @Test
    public void testMerge() throws Exception {
        String template = "hello, ${name}!";
        Context context = ContextBuilder.of().pair("name", "world").build();

        String result = engine.merge(template, context);

        assertThat(result, is("hello, world!"));
    }

    @Test
    public void testMergeForEach() throws Exception {
        String template = "#foreach ($each in $list)$each$delim#end";
        Context context = ContextBuilder.of()
                .pair("list", Arrays.asList("one", "two", "three"))
                .pair("delim", "_").build();

        String result = engine.merge(template, context);

        assertThat(result, is("one_two_three_"));
    }

}
